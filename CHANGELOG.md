# Change Log

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.1.2]

### Changed
- Osmium Oled: editor.lineHighlightBorder: removed line highlight border...

## [1.1.1]

### Fixed
- Fix theme name

## [1.1.0]

### Added
- Osmium Oled: new theme thought for oled

### Changed
- Osmium: editorCursor.foreground: editor cursor color changed to green. (we always wanna see him xD)
- Osmium: editor.lineHighlightBorder: removed line highlight border. It was horrible with column selection :O
- Osmium: editor.selectionHighlightBorder: removed border from selection
- Osmium: editor.wordHighlightBackground: finally I can see what I want select xD


## [1.0.0]

- Initial release
